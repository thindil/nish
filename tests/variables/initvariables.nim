discard """
  exitcode: 0
"""

import std/tables
import ../../src/[commandslist, nish, variables, resultcode]
import utils/helpers

var
  (db, helpContent) = initTest()
  commands = newTable[string, CommandData]()
initVariables(helpContent, db, commands)
quitShell(ResultCode(QuitSuccess), db)
